import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kozee/pages/splash_page.dart';
import 'package:kozee/providers/space_provider.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(Kozee());
}

class Kozee extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => SpaceProvider(),
      child: MaterialApp(
        home: SplashPage(),
        theme: ThemeData(
          textTheme: GoogleFonts.poppinsTextTheme(),
        ),
      ),
    );
  }
}
