import 'package:flutter/material.dart';
import 'package:kozee/pages/home_page.dart';
import 'package:kozee/pages/inbox_page.dart';
import 'package:kozee/pages/profile_page.dart';
import 'package:kozee/pages/booking_page.dart';
import 'package:kozee/services/theme.dart';

class NavBar extends StatefulWidget {
  NavBar({Key key}) : super(key: key);

  @override
  _NavBarState createState() => _NavBarState();
}

class _NavBarState extends State<NavBar> {
  PageController _pageController = PageController();

  List<Widget> _screens = [
    HomePage(),
    InboxPage(),
    TransactionPage(),
    ProfilePage()
  ];

  int _selectedIndex = 0;

  void _onPageChanged(int index) {
    setState(
      () {
        _selectedIndex = index;
      },
    );
  }

  void _onItemTapped(int selectedIndex) {
    _pageController.jumpToPage(selectedIndex);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        key: Key("pageView"),
        controller: _pageController,
        children: _screens,
        onPageChanged: _onPageChanged,
        physics: NeverScrollableScrollPhysics(),
      ),
      bottomNavigationBar: ClipRRect(
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(15),
          topLeft: Radius.circular(15),
        ),
        child: BottomNavigationBar(
          key: Key("navigationBarItems"),
          unselectedItemColor: Colors.grey,
          selectedItemColor: purpleColor,
          selectedFontSize: 12.0,
          backgroundColor: Colors.white,
          elevation: 10.0,
          type: BottomNavigationBarType.fixed,
          onTap: _onItemTapped,
          currentIndex: _selectedIndex,
          items: [
            BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage("assets/images/bottom-icon-home.png"),
              ),
              label: 'Home',
            ),
            BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage("assets/images/bottom-icon-mail.png"),
              ),
              label: 'Inbox',
            ),
            BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage("assets/images/bottom-icon-card.png"),
              ),
              label: 'Bookings',
            ),
            BottomNavigationBarItem(
              icon: ImageIcon(
                AssetImage("assets/images/user-icon-2.png"),
                size: 20,
              ),
              label: 'Profile',
            ),
          ],
        ),
      ),
    );
  }
}
