import 'package:flutter/material.dart';
import 'package:kozee/models/tips.dart';
import 'package:kozee/services/theme.dart';

class TipsCard extends StatelessWidget {
  final Tips tips;

  TipsCard(this.tips);
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Image.asset(
          tips.imageUrl,
          width: 80.0,
        ),
        SizedBox(width: 16),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              tips.title,
              style: blackTextStyle.copyWith(
                fontSize: 16,
              ),
              overflow: TextOverflow.ellipsis,
            ),
            SizedBox(height: 4),
            Text(
              'Updated ${tips.updatedAt}',
              style: TextStyle(
                fontSize: 14,
                color: greyColor,
              ),
            ),
          ],
        ),
        Spacer(),
        IconButton(
          onPressed: () {},
          icon: Icon(Icons.chevron_right),
          color: greyColor,
        ),
      ],
    );
  }
}
