import 'package:flutter/material.dart';
import 'package:kozee/models/city.dart';
import 'package:kozee/services/theme.dart';

class CityCard extends StatelessWidget {
  final City city;

  CityCard(this.city);
  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(18.0),
      child: Container(
        height: 150.0,
        width: 120.0,
        color: Colors.grey[200],
        child: Column(
          children: <Widget>[
            Stack(
              children: [
                Image.asset(
                  city.imageUrl,
                  width: 120.0,
                  height: 102.0,
                  fit: BoxFit.cover,
                ),
                city.isPopular
                    ? Align(
                        alignment: Alignment.topRight,
                        child: Container(
                          width: 50.0,
                          height: 30.0,
                          decoration: BoxDecoration(
                            color: purpleColor,
                            borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(36.0),
                            ),
                          ),
                          child: Center(
                            child: Image.asset(
                              'assets/images/icon-star.png',
                              width: 22.0,
                              height: 22.0,
                            ),
                          ),
                        ),
                      )
                    : Container(),
              ],
            ),
            SizedBox(height: 11),
            Text(
              city.name,
              style: blackTextStyle.copyWith(
                fontSize: 16,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
