import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:kozee/models/space.dart';
import 'package:kozee/pages/detail_page.dart';
import 'package:kozee/services/theme.dart';

class SpaceCard extends StatelessWidget {
  final Space space;
  SpaceCard(this.space);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => DetailPage(space),
          ),
        );
      },
      child: Row(
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.circular(18.0),
            child: Container(
              width: 130.0,
              height: 110.0,
              child: Stack(
                children: <Widget>[
                  Image.network(
                    space.imageUrl,
                    width: 130.0,
                    height: 110.0,
                    fit: BoxFit.cover,
                  ),
                  Align(
                    alignment: Alignment.topRight,
                    child: Container(
                      width: 70.0,
                      height: 25.0,
                      decoration: BoxDecoration(
                        color: purpleColor,
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(36.0),
                        ),
                      ),
                      child: Center(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.asset(
                              'assets/images/icon-star.png',
                              width: 22.0,
                              height: 22.0,
                            ),
                            Text(
                              '${space.rating}/5',
                              style: TextStyle(
                                fontSize: 13.0,
                                fontWeight: FontWeight.w500,
                                color: Colors.white,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          SizedBox(width: 20),
          Container(
            child: Flexible(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    space.name,
                    style: blackTextStyle.copyWith(
                      fontSize: 18,
                    ),
                  ),
                  SizedBox(height: 2),
                  Text.rich(
                    TextSpan(
                      text: '\$${space.price}',
                      style: TextStyle(
                        color: Color(0xFF5843BE),
                        fontSize: 16,
                      ),
                      children: [
                        TextSpan(
                          text: '/ month',
                          style: TextStyle(
                            color: Color(0xFF82868E),
                            fontSize: 16,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 16),
                  Text(
                    '${space.city}, ${space.country}',
                    style: TextStyle(
                      color: Color(0xFF82868E),
                      fontSize: 14,
                    ),
                    overflow: TextOverflow.visible,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
