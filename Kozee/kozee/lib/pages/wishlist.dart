import 'package:flutter/material.dart';
import 'package:kozee/services/theme.dart';

class Wishlist extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Wishlist",
          style: blackTextStyle.copyWith(
            fontSize: 18,
          ),
        ),
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
      ),
      body: SafeArea(
        bottom: false,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset(
                'assets/images/sorry-logo.png',
                height: 200.0,
              ),
              SizedBox(height: 50),
              Text(
                'What do you like? ',
                style: blackTextStyle.copyWith(
                  fontSize: 24,
                ),
              ),
              SizedBox(height: 14),
              Text(
                'Go to Homepage to find your\nKozee places!',
                style: TextStyle(
                  color: Color(0xFF82868E),
                  fontSize: 16,
                  fontWeight: FontWeight.w300,
                ),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 50),
            ],
          ),
        ),
      ),
    );
  }
}
