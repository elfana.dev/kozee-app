import 'package:flutter/material.dart';
import 'package:kozee/models/space.dart';
import 'package:kozee/models/tips.dart';
import 'package:kozee/providers/space_provider.dart';
import 'package:kozee/services/theme.dart';
import 'package:kozee/widgets/city_card.dart';
import 'package:kozee/models/city.dart';
import 'package:kozee/widgets/space_card.dart';
import 'package:kozee/widgets/tips_card.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    var spaceProvider = Provider.of<SpaceProvider>(context);

    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        bottom: false,
        child: ListView(
          // TITLE/HEADER
          children: <Widget>[
            SizedBox(height: edge),
            Row(
              children: [
                Padding(
                  padding: EdgeInsets.only(left: edge),
                  child: Text(
                    "Explore Now",
                    style: blackTextStyle.copyWith(
                      fontSize: 24,
                    ),
                  ),
                ),
                Spacer(),
                Padding(
                  padding: EdgeInsets.only(right: 20.0),
                  child: ImageIcon(
                    AssetImage("assets/images/bottom-icon-love.png"),
                    color: purpleColor,
                  ),
                ),
              ],
            ),
            SizedBox(height: 2),
            Padding(
              padding: EdgeInsets.only(left: edge),
              child: Text(
                "Finding the Kozee-est place",
                style: TextStyle(
                  fontSize: 16,
                  color: Color(0xFF82868E),
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
            SizedBox(height: 30),
            // POPULAR CITIES
            Padding(
              padding: EdgeInsets.only(left: edge),
              child: Text(
                "Popular Cities",
                style: regularTextStyle.copyWith(
                  fontSize: 16,
                ),
              ),
            ),
            SizedBox(height: 16),
            Container(
              height: 150.0,
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: <Widget>[
                  SizedBox(width: 24),
                  CityCard(
                    City(
                      id: 1,
                      name: 'Kuala Lumpur',
                      imageUrl: 'assets/images/kuala-lumpur.jpg',
                      isPopular: true,
                    ),
                  ),
                  SizedBox(width: 20),
                  CityCard(
                    City(
                      id: 4,
                      name: 'Jakarta',
                      imageUrl: 'assets/images/jakarta-city.png',
                      isPopular: true,
                    ),
                  ),
                  SizedBox(width: 20),
                  CityCard(
                    City(
                      id: 3,
                      name: 'Georgetown',
                      imageUrl: 'assets/images/georgetown-penang.jpg',
                    ),
                  ),
                  SizedBox(width: 20),
                  CityCard(
                    City(
                      id: 2,
                      name: 'Johor Bahru',
                      imageUrl: 'assets/images/johor-bahru.jpg',
                    ),
                  ),
                  SizedBox(width: 20),
                  CityCard(
                    City(
                      id: 5,
                      name: 'Bali',
                      imageUrl: 'assets/images/bali-city.png',
                    ),
                  ),
                  SizedBox(width: 20),
                  CityCard(
                    City(
                      id: 6,
                      name: 'Surabaya',
                      imageUrl: 'assets/images/surabaya-city.png',
                    ),
                  ),
                  SizedBox(width: 24),
                ],
              ),
            ),
            SizedBox(height: 30),
            // RECOMMENDED SPACE
            Padding(
              padding: EdgeInsets.only(left: edge),
              child: Text(
                "Recommended Space",
                style: regularTextStyle.copyWith(
                  fontSize: 16,
                ),
              ),
            ),
            SizedBox(height: 16),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: edge),
              child: FutureBuilder(
                future: spaceProvider.getRecommendedSpaces(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    List<Space> data = snapshot.data;
                    int index = 0;

                    return Column(
                      children: data.map((item) {
                        index++;
                        return Container(
                          margin: EdgeInsets.only(
                            top: index == 1 ? 0 : 30.0,
                          ),
                          child: SpaceCard(item),
                        );
                      }).toList(),
                    );
                  }
                  return Center(
                    child: CircularProgressIndicator(
                      backgroundColor: purpleColor,
                    ),
                  );
                },
              ),
            ),
            SizedBox(height: 30),
            // TIPS & GUIDANCE
            Padding(
              padding: EdgeInsets.only(left: edge),
              child: Text(
                "Tips & Guidance",
                style: regularTextStyle.copyWith(
                  fontSize: 16,
                ),
              ),
            ),
            SizedBox(height: 16),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: edge),
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    TipsCard(
                      Tips(
                        id: 1,
                        title: 'City Guidelines',
                        imageUrl: 'assets/images/city-guideline.png',
                        updatedAt: '20 April',
                      ),
                    ),
                    SizedBox(height: 20),
                    TipsCard(
                      Tips(
                        id: 2,
                        title: 'Kuala Lumpur Fairship',
                        imageUrl: 'assets/images/event.png',
                        updatedAt: '10 July',
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(height: 50 + edge),
          ],
        ),
      ),
    );
  }
}
