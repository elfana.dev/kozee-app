import 'package:flutter/material.dart';
import 'package:kozee/services/theme.dart';
import 'package:kozee/widgets/inbox_card.dart';

class InboxPage extends StatefulWidget {
  @override
  _InboxPageState createState() => _InboxPageState();
}

class _InboxPageState extends State<InboxPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Inbox",
          style: blackTextStyle.copyWith(
            fontSize: 18,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
      ),
      body: SafeArea(
        bottom: false,
        child: SingleChildScrollView(
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(height: 15),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10.0),
                  child: Text(
                    "Your chats",
                    style: blackTextStyle.copyWith(
                      fontSize: 18,
                    ),
                  ),
                ),
                InboxCard(
                  pictureUrl: 'assets/images/cozy-app-logo.png',
                  sender: "Kozee Teams",
                  time: "11.05 AM",
                  text:
                      "Hi! Welcome to Kozee, start searching for your kozee-est place now!",
                ),
                Divider(
                  color: Color(0xFFCCCCCC),
                  height: 1,
                  thickness: 1,
                  indent: 60,
                  endIndent: 0,
                ),
                InboxCard(
                  pictureUrl: 'assets/images/profile-icon.png',
                  sender: "Grand Hyatt",
                  time: "11/20",
                  text: "Thank you for your stay!",
                ),
                Divider(
                  color: Color(0xFFCCCCCC),
                  height: 1,
                  thickness: 1,
                  indent: 60,
                  endIndent: 0,
                ),
                InboxCard(
                  pictureUrl: 'assets/images/profile-picture.png',
                  sender: "Sanctuary Home",
                  time: "07/20",
                  text: "Thank you for your stay!",
                ),
                Divider(
                  color: Color(0xFFCCCCCC),
                  height: 1,
                  thickness: 1,
                  indent: 60,
                  endIndent: 0,
                ),
                InboxCard(
                  pictureUrl: 'assets/images/profile-picture.png',
                  sender: "Marriot KL",
                  time: "12/20",
                  text: "Thank you for your stay!",
                ),
                Divider(
                  color: Color(0xFFCCCCCC),
                  height: 1,
                  thickness: 1,
                  indent: 60,
                  endIndent: 0,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
