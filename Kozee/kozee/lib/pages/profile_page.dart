import 'package:flutter/material.dart';
import 'package:kozee/pages/faq_page.dart';
import 'package:kozee/pages/wishlist.dart';
import 'package:kozee/services/theme.dart';
import 'package:kozee/pages/feedback.dart';
import 'package:kozee/pages/my_profile.dart';
import 'package:kozee/pages/splash_page.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade200,
      appBar: AppBar(
        title: Text(
          "My Account",
          style: blackTextStyle.copyWith(
            fontSize: 18,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Card(
              elevation: 8.0,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              margin: const EdgeInsets.all(13.0),
              color: purpleColor,
              child: ListTile(
                title: Text(
                  "My Profile",
                  style: TextStyle(
                    fontSize: 16,
                    color: Colors.white,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                trailing: IconButton(
                  icon: Icon(Icons.edit, color: Colors.white),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => MyProfile(),
                      ),
                    );
                  },
                ),
              ),
            ),
            SizedBox(height: 10.0),
            Card(
              elevation: 4.0,
              margin: const EdgeInsets.fromLTRB(20.0, 8.0, 20.0, 16.0),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: Column(
                children: <Widget>[
                  ListTile(
                    leading: Icon(
                      Icons.message_outlined,
                      color: purpleColor,
                    ),
                    title: Text(
                      "Feedback",
                      style: TextStyle(color: Colors.black),
                    ),
                    trailing: Icon(Icons.keyboard_arrow_right),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => FeedbackPage(),
                        ),
                      );
                    },
                  ),
                  Container(
                    width: double.infinity,
                    height: 1.0,
                    color: Colors.grey,
                  ),
                  ListTile(
                    leading: ImageIcon(
                      AssetImage("assets/images/icon-love.png"),
                      color: purpleColor,
                    ),
                    title: Text(
                      "Wishlist",
                      style: TextStyle(color: Colors.black),
                    ),
                    trailing: Icon(Icons.keyboard_arrow_right),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => Wishlist(),
                        ),
                      );
                    },
                  ),
                  Container(
                    width: double.infinity,
                    height: 1.0,
                    color: Colors.grey,
                  ),
                  ListTile(
                    leading: Icon(
                      Icons.question_answer_outlined,
                      color: purpleColor,
                    ),
                    title: Text(
                      "FAQ",
                      style: TextStyle(color: Colors.black),
                    ),
                    trailing: Icon(Icons.keyboard_arrow_right),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => FaqPage(),
                        ),
                      );
                    },
                  ),
                  Container(
                    width: double.infinity,
                    height: 1.0,
                    color: Colors.grey,
                  ),
                  ListTile(
                    leading: Icon(Icons.logout, color: Colors.deepPurple),
                    title: Text(
                      "Sign Out",
                      style: TextStyle(color: Colors.black),
                    ),
                    trailing: Icon(Icons.keyboard_arrow_right),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => SplashPage(),
                        ),
                      );
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
