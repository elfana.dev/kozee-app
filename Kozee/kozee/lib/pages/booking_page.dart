import 'package:flutter/material.dart';
import 'package:kozee/services/theme.dart';
import 'package:kozee/widgets/booking_card.dart';

class TransactionPage extends StatefulWidget {
  @override
  _TransactionPageState createState() => _TransactionPageState();
}

class _TransactionPageState extends State<TransactionPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "My Bookings",
          style: blackTextStyle.copyWith(
            fontSize: 18,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
      ),
      body: SafeArea(
        bottom: false,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(vertical: edge),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                _formTitle(
                  "Upcoming",
                ),
                SizedBox(height: 10),
                //     Container(
                //       width: 150.0,
                //       height: 2.0,
                //       color: Colors.grey,
                //     ),
                SizedBox(height: 50),
                Container(
                  padding: EdgeInsets.symmetric(vertical: edge),
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Image.asset(
                          'assets/images/not-found-logo.png',
                          width: 300.0,
                        ),
                        SizedBox(height: 40),
                        Text(
                          'You have no upcoming booking',
                          style: blackTextStyle.copyWith(
                            fontSize: 18,
                          ),
                        ),
                        SizedBox(height: 14),
                        Text(
                          'Start exploring your next trip!',
                          style: TextStyle(
                            color: Color(0xFF82868E),
                            fontSize: 16,
                            fontWeight: FontWeight.w300,
                          ),
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(height: 50),
                      ],
                    ),
                  ),
                ),
                Divider(
                  color: Colors.grey[200],
                  height: 10,
                  thickness: 15,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: edge),
                  child: _formTitle(
                    "Past Bookings",
                  ),
                ),
                BookingCard(
                  date: "1 July 2020",
                  place: "Sanctuary Home",
                ),
                SizedBox(height: 20),
                BookingCard(
                  date: "15 November 2020",
                  place: "Grand Hyatt",
                ),
                SizedBox(height: 20),
                BookingCard(
                  date: "20 December 2020",
                  place: "Marriot KL",
                ),
                SizedBox(height: 60),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

Widget _formTitle(String title) {
  return Container(
    padding: EdgeInsets.symmetric(horizontal: edge),
    child: Text(
      title,
      style: blackTextStyle.copyWith(
        fontSize: 18,
        fontWeight: FontWeight.w600,
      ),
    ),
  );
}
