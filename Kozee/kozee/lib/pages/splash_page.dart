import 'package:flutter/material.dart';
import 'package:kozee/widgets/bottom_navbar_item.dart';
import 'package:kozee/services/theme.dart';

class SplashPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        bottom: false, // applicable for iphone x and so on
        child: Stack(
          children: [
            Align(
              alignment: Alignment.bottomCenter,
              child: Image.asset("assets/images/splash-image.png"),
            ),
            Padding(
              padding: EdgeInsets.only(top: 50.0, left: 30.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Image(
                        image: AssetImage(
                          "assets/images/cozy-app-logo.png",
                        ),
                        width: 50,
                        height: 50,
                      ),
                      SizedBox(width: 20),
                      Text(
                        "Kozee",
                        style: TextStyle(
                          fontSize: 35.0,
                          fontWeight: FontWeight.w600,
                          color: Colors.deepOrange,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 30),
                  Text(
                    "Find Cozy House\nto Stay and Happy",
                    style: TextStyle(
                      fontSize: 24.0,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(height: 10),
                  Text(
                    "Stop wasting so much time\nin a place that is not habitable",
                    style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.w300,
                      color: greyColor,
                    ),
                  ),
                  SizedBox(height: 40),
                  Container(
                    width: 210.0,
                    height: 50.0,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(17),
                      ),
                      color: purpleColor,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => NavBar(),
                          ),
                        );
                      },
                      child: Text(
                        "Explore Now",
                        style: TextStyle(
                          fontSize: 18,
                          color: whiteColor,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
